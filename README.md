# k3s_rpi


Install k3s on all raspi's:

1. Add ssh key on pi's with [hypriot](https://blog.hypriot.com/downloads) image installed
2. change cluster_secret and ./k3s_installation.sh
3. Copy k3s config file from k3s_master/etc/rancher/k3s/k3s.yaml to kubectl client e.g. to your_laptop:~/k8s_configs/pi
4. export KUBECONFIG=~/k8s_configs/pi

Install Prometheus-operator on the pi's. Original repo and all work is done by this guy https://github.com/carlosedp/cluster-monitoring

5. kubectl apply -f prometheus-operator/manifests (wait 1 min for the crd's to load properly)
6. kubectl apply -f prometheus-operator/manifests
7. watch kubectl get po,svc,deploy,ds,ing -o wide and enjoy
