#!/bin/bash

# Installation of k3s cluster
# Created 18th of March 2019 by Gino Eising

SSH_OPT="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o CheckHostIP=no" 
CLUSTER_SECRET=somethingtotallyrandomdflkjdflkdjgldkjgibjvlfjl
MASTER="https://node01.lan:6443"

for i in 1 2 3 4 5 6 7 8 9
do
	HOSTNAME=$(ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pirate@node0$i.lan "hostname")
	if [[ $HOSTNAME = node01 ]];then

		printf "command should run on node01\n"
		ssh $SSH_OPT pirate@node01.lan "sudo /usr/local/bin/k3s-uninstall.sh;sudo /usr/local/bin/k3s-agent-uninstall.sh;rm -fr ~/.kube;sudo systemctl stop k3s.service"
	else
		printf "command should run on node0$i\n"
		ssh $SSH_OPT pirate@node0$i.lan "sudo /usr/local/bin/k3s-agent-uninstall.shi;sudo systemctl stop k3s.service"
	fi
done

for i in 1 2 3 4 5 6 7 8 9
do
        HOSTNAME=$(ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pirate@node0$i.lan "hostname")
        echo $HOSTNAME
        if [[ $HOSTNAME = node01 ]];then
                printf "command should run on node01\n"
		ssh $SSH_OPT pirate@node01.lan "curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="server" K3S_URL=$MASTER K3S_CLUSTER_SECRET=$CLUSTER_SECRET sh -"
        else
                printf "command should run on node0$i\n"
		ssh $SSH_OPT pirate@node0$i.lan "curl -sfL https://get.k3s.io | K3S_URL=$MASTER K3S_CLUSTER_SECRET=$CLUSTER_SECRET sh -"
        fi
done

rsync -avz -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress k3s_manifests/ pirate@node01.lan:~/k3s_manifests

while ! nc -z node01.lan 6443
do
	ssh $SSH_OPT pirate@node01.lan "sudo systemctl restart k3s.service"
        STATUS_DJIENO=$(kubectl rollout status deploy/$1 | awk  '{print $3}')

	for i in regsecret.yml develop.chebna.eu.yml develop.djieno.com.latest.yml master.chebna.eu.yml master.djieno.com.latest.yml
	do
		ssh $SSH_OPT pirate@node01.lan "kubectl apply -f ~/k3s_manifests/$i"

	done

        if [ "$STATUS_DJIENO" == "successfully" ] ; then

                printf "$1 deployed, testing now... \n\n"
                break
        fi

		sleep 3
done








